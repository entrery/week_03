package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {
    private ArraySimpleQueue<String> list;


    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleQueue<>(3);
        list.offer("1");
        list.offer("2");

    }

    @Test
    public void offer() {
        list.offer("3");

    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testOffer() {
        list.offer("4");
        list.offer("5");
    }

    @Test
    public void poll() {
        list.poll();
        assertEquals(1, list.size());
    }

    @Test(expected = NullPointerException.class)
    public void testPoll() {
        list.poll();
        list.poll();
        list.poll();
    }

    @Test
    public void peek() {
        assertEquals("1", list.peek());
    }

    @Test(expected = NullPointerException.class)
    public void testPeek() {
        list.poll();
        list.poll();
        list.peek();
    }

    @Test
    public void size() {
        assertEquals(2, list.size());
    }

    @Test
    public void capacity() {
        assertEquals(1, list.capacity() );
    }
}