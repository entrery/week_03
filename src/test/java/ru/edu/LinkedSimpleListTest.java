package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleListTest {

    private SimpleList<String> listTest ;

    @Before
    public void setUp() throws Exception {
        listTest = new LinkedSimpleList<>();
        listTest.add("1");
        listTest.add("2");
        listTest.add("3");

    }

    @Test
    public void add() {
        assertEquals(3, listTest.size());
        listTest.add("4");
        assertEquals(4, listTest.size());
    }

    @Test
    public void set() {
        listTest.set(1,"10");
        assertEquals("10", listTest.get(1));
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void TestIndexSet(){
        listTest.set(4,"10");
    }


    @Test
    public void get() {
        assertEquals("2", listTest.get(1) );

    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndex() {
        listTest.get(5);
    }


    @Test
    public void remove() {
        listTest.remove(2);
        assertEquals(2, listTest.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexRemove(){
        listTest.remove(4);
    }


    @Test
    public void indexOf() {
        assertEquals(2, listTest.indexOf("3"));

    }

    @Test
    public void size() {
        assertEquals(3, listTest.size());
    }
}