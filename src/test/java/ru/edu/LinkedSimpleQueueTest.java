package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {

    private LinkedSimpleQueue queue;

    @Before
    public void setUp() throws Exception {
        queue = new LinkedSimpleQueue(3);
        queue.offer("1");
        queue.offer("2");
    }

    @Test
    public void offer() {
        queue.offer("3");
        assertEquals(3, queue.size());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
     public void testOffer(){
        queue.offer("3");
        queue.offer("4");
    }

    @Test
    public void poll() {
        queue.poll();
        assertEquals(2, queue.capacity());
    }

    @Test(expected = NullPointerException.class)
    public void testPoll(){
        queue.poll();
        queue.poll();
        queue.poll();
    }

    @Test
    public void peek() {
        assertEquals("1", queue.peek());
    }

    @Test(expected = NullPointerException.class)
    public void testPeek(){
        queue.poll();
        queue.poll();
        queue.peek();
    }

    @Test
    public void size() {
        assertEquals(3, queue.size());
    }

    @Test
    public void capacity() {
        assertEquals(1, queue.capacity());
    }
}