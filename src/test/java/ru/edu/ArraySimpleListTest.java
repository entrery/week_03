package ru.edu;

import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class ArraySimpleListTest {

    private ArraySimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleList<>(3);
        list.add("1");
        list.add("2");
        list.add("3");
    }

    @Test
    public void add() {
    list.add("4");
    assertEquals(4, list.size());
    }


    @Test
    public void set() {
        list.set(1, "5");
        assertEquals("5", list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSet() {
        list.set(10,"10");
    }

    @Test
    public void get() {
        assertEquals("1", list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGet() {
        list.get(10);
    }

    @Test
    public void remove() {
        list.remove(0);
        assertEquals("2",list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemove() {
        list.remove(10);
    }

    @Test
    public void indexOf() {
        assertEquals(1, list.indexOf("2"));
    }

    @Test
    public void size() {
        assertEquals(3, list.size());
    }
}
