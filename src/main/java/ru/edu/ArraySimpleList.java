package ru.edu;


import java.util.Arrays;

public class ArraySimpleList<T> implements SimpleList<T> {
    /**
     * массив.
     */
    private T[] arr;

    /**
     * размер.
     */
    private int size;

    /**
     * емкость.
     *
     * @param capacity
     */
    public ArraySimpleList(final int capacity) {

        this.arr = (T[]) new Object[capacity];
    }


    /**
     * Добавление элемента в конец списка.
     */
    @Override
    public void add(final T value) {
        if (size + 1 == arr.length) {
            T[] old = arr;
            this.arr = (T[]) new Object[arr.length * 2];
            for (int i = 0; i < old.length; i++) {
                arr[i] = old[i];
            }
            arr = Arrays.copyOf(arr, arr.length * 2);
        }
        arr[size++] = value;
    }


    /**
     * Проверка.
     *
     * @param index индекс
     */
    public void findIndex(final int index) {
        if (index >= size || index < 0) {

            throw new IndexOutOfBoundsException("Неверный индекс.");
        }
    }






    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        findIndex(index);
        arr[index] = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        findIndex(index);
        return arr[index];
    }


    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        findIndex(index);
        for (int i = index + 1; i < size; i++) {
            arr[i - 1] = arr[i];
        }
        arr[size - 1] = null;

    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        for (int i = 0; i < size; i++) {
            if (arr[i].equals(value)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
