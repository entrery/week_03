package ru.edu;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {
    /**
     * массив.
     */
    private T[] array;

    /**
     * голова.
     */
    private int head = 0;

    /**
     * хвост.
     */
    private int tail = 0;

    /**
     * размер.
     */
    private int size;

    /**
     * массив.
     */
    private int count = 0;

    ArraySimpleQueue(final int capacity) {
        array = (T[]) new Object[capacity];
        size = capacity;
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (count == size) {
            throw new ArrayIndexOutOfBoundsException("Очередь заполнена");
        } else {
            count++;
            array[tail] = value;
            tail = (tail + 1) % size;

            return true;
        }
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (count == 0) {
            throw new NullPointerException("Очередь пуста. ");
        }

        T elem = array[head];
        array[head] = null;
        head = (head + 1) % size;
        --count;
        return elem;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (count == 0) {
            throw new NullPointerException("Очередь пуста. ");
        }

        return array[head];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        int cap = size - count;
        return cap;
    }
}
