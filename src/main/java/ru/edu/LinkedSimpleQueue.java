package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * голова очереди.
     */
    private Node<T> head;

    /**
     * хвост очереди.
     */
    private Node<T> tail;

    /**
     * размер.
     */
    private int size;

    /**
     * счет.
     */
    private int count;

    /**
     * @param capacity
     */
    LinkedSimpleQueue(final int capacity) {
        size = capacity;
    }

    /**
     * @param <T>
     */
    private static class Node<T> {

        /**
         * указатель предыдущий.
         */
        private Node prev;

        /**
         * ткущее значение.
         */
        private T value;

        /**
         * указатель следующий.
         */
        private Node next;
        Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (count == size) {
            throw new ArrayIndexOutOfBoundsException("Очередь заполнена. ");
        }

        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
            tail = node;
        } else {

            node.prev = tail;
            tail.next = node;
            tail = node;
        }

        count++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (count == 0) {
            throw new NullPointerException("Очередь пуста. ");
        }
        T elem = head.value;
        head = head.next;
        count--;
        if (count == 0) {
            tail = null;
        }
        return elem;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (count == 0) {
            throw new NullPointerException("Очередь пуста. ");
        }

        return head.value;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return size - count;
    }
}

